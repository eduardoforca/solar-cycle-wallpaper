import QtQuick 2.0
import QtQml 2.15
import QtQuick.Layouts 1.1
import org.kde.plasma.core 2.0 as PlasmaCore

Item {
    id: sunDataRoot
	PlasmaCore.DataSource {
        id: location
        engine: "geolocation"
        connectedSources: ["location"]
        interval: 500
        property bool isReady: false
		onNewData: {
            isReady = true
		}
    }

    PlasmaCore.DataSource {
        id: dataSource
        engine: "time"
        connectedSources: ["Local"]
        interval: 500
    }


    property bool enableFetch: true
    property var longitude: 0
    property var latitude: 0

	property var locLat: latitude
	property var locLon: longitude
    
    Binding {
        target: sunDataRoot
        property: "locLat"
        value: isReady ? location.data.location.latitude : latitude
        when: location.isReady && enableFetch
        restoreMode: Binding.RestoreBinding
    }
    Binding {
        target: sunDataRoot
        property: "locLon"
        value: isReady ? location.data.location.longitude : longitude
        when: location.isReady && enableFetch
        restoreMode: Binding.RestoreBinding
    }

    readonly property string timeString: dataSource.data.Local.DateTime.toISOString()
    readonly property string solarString: `UTC|Solar|Latitude=${locLat}|Longitude=${locLon}|DateTime=${timeString}`
    property var pathSinceSunrise: 0

    PlasmaCore.DataSource {
        id: dataSourceSolar
        engine: "time"
        connectedSources: [solarString]
        interval: 0
        onNewData: {
            var angle = Math.atan2(data["Corrected Elevation"], 180 - data.Azimuth) * 180 / Math.PI
            pathSinceSunrise = (angle > 0 ? angle : angle + 360) / 360
        }
    }
}
