/*
 *  Copyright 2013 Marco Martin <mart@kde.org>
 *  Copyright 2014 Kai Uwe Broulik <kde@privat.broulik.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  2.010-1301, USA.
 */

import QtQuick 2.5
import QtQuick.Controls 1.0 as QtControls
import QtQuick.Controls 2.3 as QtControls2
import QtQuick.Layouts 1.0
import QtQml.Models 2.15
import QtQuick.Window 2.0 // for Screen
//We need units from it
import org.kde.plasma.core 2.0 as Plasmacore
import org.kde.plasma.wallpapers.image 2.0 as Wallpaper
import org.kde.kquickcontrols 2.0 as KQuickControls
import org.kde.kquickcontrolsaddons 2.0
import org.kde.kconfig 1.0 // for KAuthorized
import org.kde.draganddrop 2.0 as DragDrop
import org.kde.kcm 1.1 as KCM
import org.kde.kirigami 2.5 as Kirigami
import '../code/naturalSort.js' as Sorter

ColumnLayout {
    id: root
    property alias cfg_Color: colorButton.color
    // property string cfg_Image
    property int cfg_FillMode
    property int cfg_FirstPictureHour
    property alias cfg_Blur: blurRadioButton.checked
    property alias cfg_UseLocation: solarCheckBox.checked
    property alias cfg_AutomaticLocation: autoLocationCheckBox.checked
    property alias cfg_Longitude: longitudeSpin.value
    property alias cfg_Latitude: latitudeSpin.value
    property var cfg_UncheckedSlides: []
    property var cfg_UncheckedSlidesDefault: []
    property var cfg_SlidePaths: ""
    property var cfg_DefaultImage: ""
    property int cfg_TransitionAnimationDuration: 400

    property var checkedSlides: []

    function saveConfig() {
        imageWallpaper.commitDeletion();
    }

    SystemPalette {
        id: syspal
    }

    Wallpaper.Image {
        id: imageWallpaper
        targetSize: {
            if (typeof plasmoid !== "undefined") {
                return Qt.size(plasmoid.width, plasmoid.height)
            }
            // Lock screen configuration case
            return Qt.size(Screen.width, Screen.height)
        }
        onSlidePathsChanged: {
            cfg_SlidePaths = slidePaths
            uncheckedSlides = []
            checkedSlides = []
        }
        onUncheckedSlidesChanged: cfg_UncheckedSlides = uncheckedSlides
    }

    onCheckedSlidesChanged: {
        cfg_DefaultImage = checkedSlides[0]
    }

    onCfg_SlidePathsChanged: {
        if (cfg_SlidePaths.length > 1) {
            cfg_SlidePaths = [cfg_SlidePaths[1]]
        } else {
            imageWallpaper.slidePaths = cfg_SlidePaths
        }
    }

    onCfg_UncheckedSlidesChanged: {
        imageWallpaper.uncheckedSlides = cfg_UncheckedSlides
    }

    //Rectangle { color: "orange"; x: formAlignment; width: formAlignment; height: 20 }

    TextMetrics {
        id: textMetrics
        text: "00"
    }

    Row {
        spacing: units.largeSpacing / 2
        QtControls2.Label {
            id: positionLabel
            width: formAlignment - units.largeSpacing
            anchors {
                verticalCenter: resizeComboBox.verticalCenter
            }
            text: i18nd("plasma_wallpaper_org.kde.image", "Positioning:")
            horizontalAlignment: Text.AlignRight
        }

        // TODO: port to QQC2 version once we've fixed https://bugs.kde.org/show_bug.cgi?id=403153
        QtControls2.ComboBox {
            id: resizeComboBox
            TextMetrics {
                id: resizeTextMetrics
                text: resizeComboBox.currentText
            }
            width: resizeTextMetrics.width + Kirigami.Units.smallSpacing * 2 + Kirigami.Units.gridUnit * 2
            model: [
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "Scaled and Cropped"),
                    'fillMode': Image.PreserveAspectCrop
                },
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "Scaled"),
                    'fillMode': Image.Stretch
                },
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "Scaled, Keep Proportions"),
                    'fillMode': Image.PreserveAspectFit
                },
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "Centered"),
                    'fillMode': Image.Pad
                },
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "Tiled"),
                    'fillMode': Image.Tile
                }
            ]

            textRole: "label"
            onCurrentIndexChanged: cfg_FillMode = model[currentIndex]["fillMode"]
            Component.onCompleted: setMethod();

            function setMethod() {
                for (var i = 0; i < model.length; i++) {
                    if (model[i]["fillMode"] == wallpaper.configuration.FillMode) {
                        resizeComboBox.currentIndex = i;
                        var tl = model[i]["label"].length;
                        //resizeComboBox.textLength = Math.max(resizeComboBox.textLength, tl+5);
                    }
                }
            }
        }
    }

    QtControls2.ButtonGroup { id: backgroundGroup }

    Row {
        id: blurRow
        spacing: units.largeSpacing / 2
        visible: cfg_FillMode === Image.PreserveAspectFit || cfg_FillMode === Image.Pad
        QtControls2.Label {
            id: blurLabel
            width: formAlignment - units.largeSpacing
            anchors.verticalCenter: blurRadioButton.verticalCenter
            horizontalAlignment: Text.AlignRight
            text: i18nd("plasma_wallpaper_org.kde.image", "Background:")
        }
        QtControls2.RadioButton {
            id: blurRadioButton
            text: i18nd("plasma_wallpaper_org.kde.image", "Blur")
            QtControls2.ButtonGroup.group: backgroundGroup
        }
    }

    Row {
        id: transitionDurationRow
        spacing: units.largeSpacing / 2
        QtControls2.Label {
            width: formAlignment - units.largeSpacing
            anchors.verticalCenter: transitionDurationSpinBox.verticalCenter
            horizontalAlignment: Text.AlignRight
            text: i18n("Wallpaper transition time:")
        }
        QtControls2.SpinBox {
            id: transitionDurationSpinBox
            value: cfg_TransitionAnimationDuration
            onValueChanged: cfg_TransitionAnimationDuration = value
            from: 0
            to: 2000000000
            stepSize: 100
            editable: true

            textFromValue: function(value, locale) {
                return i18n("%1ms", value)
            }
            valueFromText: function(text, locale) {
                // Number.fromLocaleString() doesn't strip suffix and raises an error.
                // return Number.fromLocaleString(locale, text)
                
                // parseInt does seem to stip non-digit characters, but it probably
                // only works with ASCII digits?
                return parseInt(text, 10)
            }
        }
    }

    Row {
        id: colorRow
        visible: cfg_FillMode === Image.PreserveAspectFit || cfg_FillMode === Image.Pad
        spacing: units.largeSpacing / 2
        QtControls2.Label {
            width: formAlignment - units.largeSpacing
        }
        QtControls2.RadioButton {
            id: colorRadioButton
            text: i18nd("plasma_wallpaper_org.kde.image", "Solid color")
            QtControls2.ButtonGroup.group: backgroundGroup
            checked: !cfg_Blur
        }
        KQuickControls.ColorButton {
            id: colorButton
            dialogTitle: i18nd("plasma_wallpaper_org.kde.image", "Select Background Color")
        }
    }

    Plasmacore.DataSource {
        id: location
        engine: "geolocation"
        connectedSources: ["location"]
        interval: 0
		onNewData: {
            if (autoLocationCheckBox.checked) {
                longitudeSpin.value = data.longitude
                latitudeSpin.value = data.latitude
            }
		}
    }

    Row {
        spacing: units.largeSpacing / 2
        QtControls2.Label {
            width: formAlignment - units.largeSpacing
        }
        QtControls2.CheckBox {
            Layout.alignment: Qt.AlignCenter
            id: solarCheckBox
            text: i18nd("plasma_wallpaper_org.kde.image", "Base sun data on location")
            checked: cfg_UseLocation
        }
        QtControls2.CheckBox {
            Layout.alignment: Qt.AlignCenter
            id: autoLocationCheckBox
            text: i18nd("plasma_wallpaper_org.kde.image", "Use automatic location")
            checked: cfg_AutomaticLocation
            visible: solarCheckBox.checked
        }
    }
    Row {
        visible: solarCheckBox.checked
        spacing: units.largeSpacing / 2
        // Layout.fillWidth: true
        QtControls2.Label {
            width: formAlignment - units.largeSpacing
        }
        QtControls2.Label {
            Layout.minimumWidth: formAlignment - units.largeSpacing
            horizontalAlignment: Text.AlignRight
            text: i18nd("plasma_wallpaper_org.kde.image","Longitude:")
        }
        QtControls.SpinBox {
            enabled: !autoLocationCheckBox.checked
            id: longitudeSpin
            // Layout.minimumWidth: textMetrics.width + units.gridUnit
            // width: units.gridUnit * 3
            value: cfg_Longitude
            minimumValue: -180.0
            maximumValue: 180.0
            decimals: 3
        }
        QtControls2.Label {
            Layout.minimumWidth: formAlignment - units.largeSpacing
            horizontalAlignment: Text.AlignRight
            text: i18nd("plasma_wallpaper_org.kde.image","Latitude:")
        }
        QtControls.SpinBox {
            enabled: !autoLocationCheckBox.checked
            id: latitudeSpin
            // Layout.minimumWidth: textMetrics.width + units.gridUnit
            // width: units.gridUnit * 3
            value: cfg_Latitude
            minimumValue: -90.0
            maximumValue: 90.0
            decimals: 3
        }
    }

    Row {
        spacing: units.largeSpacing / 2
        QtControls2.Label {
            width: formAlignment - units.largeSpacing
            anchors {
                verticalCenter: sunpositionComboBox.verticalCenter
            }
            text: i18nd("plasma_wallpaper_org.kde.image", "Sun position on first picture:")
            horizontalAlignment: Text.AlignRight
        }

        // TODO: port to QQC2 version once we've fixed https://bugs.kde.org/show_bug.cgi?id=403153
        QtControls2.ComboBox {
            id: sunpositionComboBox
            TextMetrics {
                id: sunpositionTextMetrics
                text: sunpositionComboBox.currentText
            }
            width: sunpositionTextMetrics.width + Kirigami.Units.smallSpacing * 2 + Kirigami.Units.gridUnit * 4
            model: [
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "Sunrise(~06:00)"),
                    'firstPictureHour': 0
                },
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "MidMorning(~09:00)"),
                    'firstPictureHour': 1
                },
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "Solar Noon(~12:00)"),
                    'firstPictureHour': 2
                },
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "MidAfternoon(~15:00)"),
                    'firstPictureHour': 3
                },
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "Sunset(~18:00)"),
                    'firstPictureHour': 4
                },
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "MidEvening(~21:00)"),
                    'firstPictureHour': 5
                },
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "Solar Midnight(~00:00)"),
                    'firstPictureHour': 6
                },
                {
                    'label': i18nd("plasma_wallpaper_org.kde.image", "Small Hours(~03:00)"),
                    'firstPictureHour': 7
                }
            ]

            textRole: "label"
            onCurrentIndexChanged: cfg_FirstPictureHour = model[currentIndex]["firstPictureHour"]
            Component.onCompleted: setMethod();

            function setMethod() {
                for (var i = 0; i < model.length; i++) {
                    if (model[i]["firstPictureHour"] == wallpaper.configuration.FirstPictureHour) {
                        sunpositionComboBox.currentIndex = i;
                        var tl = model[i]["label"].length;
                        //sunpositionComboBox.textLength = Math.max(sunpositionComboBox.textLength, tl+5);
                    }
                }
            }
        }
    }

    default property alias contentData: content.data
    ColumnLayout {
        id: content
    }

    Component {
        id: foldersComponent
        ColumnLayout {
            anchors.fill: parent
            GridLayout {
                columns: 2
                Layout.fillWidth: true
                Layout.fillHeight: true
                columnSpacing: Kirigami.Units.largeSpacing
                Rectangle {
                    color: "transparent"
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    Layout.minimumHeight: changeFolderButton.height
                    Rectangle {
                        id: foldersPane
                        height: parent.height
                        anchors.left: parent.left
                        anchors.right: changeFolderButton.left
                        color: Plasmacore.Theme.viewBackgroundColor
                        anchors.verticalCenter: changeFolderButton.verticalCenter
                        ListView {
                            anchors.fill: parent
                            anchors.topMargin: parent.height / 3
                            id: slidePathsView
                            model: imageWallpaper.slidePaths
                            delegate: QtControls2.Label {
                                text: modelData
                            }
                        }
                    }
                    QtControls2.Button {
                        id: changeFolderButton
                        // Layout.fillWidth: true
                        anchors.right: parent.right
                        icon.name: "list-add"
                        text: i18nd("plasma_wallpaper_org.kde.image","Select Folder…")
                        onClicked: imageWallpaper.showAddSlidePathsDialog()
                    }
                }
                Loader {
                    Layout.columnSpan: 2
                    sourceComponent: thumbnailsComponent
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    anchors.fill: undefined
                }
            }
        }
    }

    DelegateModel {
        id: orderedModel
        model: imageWallpaper.slideFilterModel
        function lessThan(left, right) {
            return Sorter.alphaNum(left.path.toString(), right.path.toString()) < 0
        }

        function insertPosition(item) {
            var lower = 0
            var upper = items.count
            while (lower < upper) {
                var middle = Math.floor(lower + (upper - lower) / 2)
                var result = lessThan(item.model, items.get(middle).model);
                if (result) {
                    upper = middle
                } else {
                    lower = middle + 1
                }
            }
            return lower
        }

        function sort() {
            while (unsortedItems.count > 0) {
                var item = unsortedItems.get(0)
                var index = insertPosition(item)

                item.groups = "items"
                items.move(item.itemsIndex, index)
            }
        }

        Component.onCompleted: {model.usedInConfig = true}

        items.includeByDefault: false
        groups: DelegateModelGroup {
            id: unsortedItems
            name: "unsorted"

            includeByDefault: true
            onChanged: {
                orderedModel.sort()
            }
        }
        delegate: WallpaperDelegate {
            color: cfg_Color
        }
    }

    Component {
        id: thumbnailsComponent
        KCM.GridView {
            id: wallpapersGrid
            anchors.fill: parent
            view.model: orderedModel
        }
    }

    Loader {
        Layout.fillWidth: true
        Layout.fillHeight: true
        sourceComponent: foldersComponent
    }

}
