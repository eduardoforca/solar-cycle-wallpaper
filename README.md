# Solar Cycle Wallpaper

https://store.kde.org/p/1667992
-----------
# About

This is a Plasma 5 Wallpaper Plugin that changes your wallpaper according to current sun position in your location.

It includes the option to enable Inactive Blur, which uses https://github.com/Zren/plasma-wallpapers/tree/master/inactiveblur implementation.
---------------
# Installation

The easiest way to install it is through KDE itself:

1. Right click the desktop and open desktop settings
2. When selecting wallpaper plugin, click to Add New Plugin
3. Search for "Solar Cycle Wallpaper" and install it
4. Choose Solar Cycle as your plugin

------------
# Setup

## Location

- System Location

    The default mode is to use your system location and clock to get the current sun position.

- Custom Location

    You can also manually input the coordinates of another location to get the current sun position at that location.

- No Location

    If you don't want to base sun data on location, the plugin will use the standard 6am as sunrise time and 6pm as sunset.

## Images

All your images must be inside the same folder and ordered alphabetically. The plugin will show the image according to the sun's position in the sky and the time shown in the first picture (as chosen in settings):

- Sunrise: The sun is at its lowest visible position going up
- Solar noon: The sun is at its highest position in the sky
- Sunset: The sun is at its lowest position visible in the sky going down
- Solar Midnight: The sun is the furthest away from the location

## Examples

- Number of images: 2.
    -   1st from sunrise to sunset
    -   2nd from sunset to sunrise.

- Number of images: 4
    -   1st from sunrise till noon
    -   2nd from noon to sunset
    -   3rd from sunset to midnight
    -   4th from midnight to sunrise
